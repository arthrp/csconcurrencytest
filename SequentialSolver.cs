using System;

public class SequentialSolver : MonteCarloCircleAreaApproximator
{
    private int _pointsInCircle = 0;
    private Random _rand = new Random();

    public override double Solve(int radius, int iterations)
    {
        int centerX, centerY;
        var squareSide = radius*2 + ADDITIONAL_SIZE;
        var totalArea = squareSide*squareSide;
        
        centerX = centerY = radius+ADDITIONAL_SIZE/2;

        for(int i=0; i<iterations; i++)
        {
            ProcessPoint(centerX,centerY,radius,squareSide);
        }

        double ratio = (double)_pointsInCircle/(double)iterations;
        double circleArea = ratio * totalArea;
        // Console.WriteLine(_pointsInCircle+ " " + iterations + " " + ratio+ " " + circleArea);

        return circleArea;
    }
    
    private void ProcessPoint(int centerX, int centerY, int radius, int sideSize)
    {
        var pointX = _rand.Next(0, sideSize);
        var pointY = _rand.Next(0, sideSize);

        var isInCircle = IsPointInCircle(pointX,pointY,centerX,centerY,radius);
        if(isInCircle)
            _pointsInCircle++;
    }
}