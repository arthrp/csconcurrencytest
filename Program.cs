﻿using System;

namespace ConcurrentTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var radius = Int32.Parse(args[0]);

            Console.WriteLine("Formula result: "+GetFormulaResult(radius));
            var approximateArea = new ConcurrentSolver().Solve(radius, 32_000_000);

            Console.WriteLine("Approximation using Monte-Carlo method: "+approximateArea);
        }

        static double GetFormulaResult(int radius)
        {
            return Math.PI * (radius*radius);
        }
    }
}
