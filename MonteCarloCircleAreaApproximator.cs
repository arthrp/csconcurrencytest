using System;

public abstract class MonteCarloCircleAreaApproximator
{
    public abstract double Solve(int radius, int iterations);

    protected const int ADDITIONAL_SIZE = 100;

    protected bool IsPointInCircle(int x, int y, int centerX, int centerY, int rad)
    {
        return Math.Pow((x - centerX), 2) + Math.Pow((y - centerY),2) < Math.Pow(rad,2);
    }
}