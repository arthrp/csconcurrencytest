using System;
using System.Threading.Tasks;

public class ConcurrentSolver : MonteCarloCircleAreaApproximator
{
    private const int CONCURRENT_TASKS = 4;

    public override double Solve(int radius, int iterations)
    {
        int centerX, centerY;
        var squareSide = radius*2 + ADDITIONAL_SIZE;
        var totalArea = squareSide*squareSide;
        
        centerX = centerY = radius+ADDITIONAL_SIZE/2;

        int iterationsPerTask = iterations/CONCURRENT_TASKS;
        Task<int>[] tasks = new Task<int>[CONCURRENT_TASKS];

        for(int taskIdx = 0; taskIdx<CONCURRENT_TASKS; taskIdx++)
        {
            tasks[taskIdx] = Task.Run(() => {
                return ProcessTask(iterationsPerTask,squareSide,centerX,centerY,radius);
            });
        }

        Task.WaitAll(tasks);
        var pointsInCircle = 0;
        foreach(var task in tasks)
            pointsInCircle += task.Result;

        double ratio = (double)pointsInCircle/(double)iterations;
        double circleArea = ratio * totalArea;
        // Console.WriteLine(res+ " " + iterations + " " + ratio+ " " + circleArea);

        return circleArea;
    }

    private int ProcessTask(int iters, int squareSide, int centerX, int centerY, int radius)
    {
        int pointsInCircle = 0;
        var r = new Random();

        for(int i=0; i<iters; i++)
        {
            var pointX = r.Next(0, squareSide);
            var pointY = r.Next(0, squareSide);
                    
            var isInCircle = IsPointInCircle(pointX,pointY,centerX,centerY,radius);
            if(isInCircle)
                pointsInCircle++;
        }
                
        return pointsInCircle;
    }
}